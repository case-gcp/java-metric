FROM maven AS build
WORKDIR /app
COPY . /app
RUN mvn clean package -DskipTests

FROM adoptopenjdk/openjdk11:alpine-slim
WORKDIR /HOME
COPY --from=build /app/target/actuator-demo-0.0.1-SNAPSHOT.jar /HOME/
#COPY target/actuator-demo-0.0.1-SNAPSHOT.jar /HOME/
EXPOSE 8080

CMD ["java", "-jar", "actuator-demo-0.0.1-SNAPSHOT.jar"]

