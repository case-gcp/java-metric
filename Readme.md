# JAVA Spring Boot - com Actuator: Health Check, e Metrics para monitora

## O que vc precisa par rodar a aplicaçāo local

- openJDK11
- maven
- Docker

## Passos para a configuração

**1. Clone da aplicaçāo**

```bash
git clone git@gitlab.com:cases-samuel/java-metrics.git
ou
git clone https://gitlab.com/cases-samuel/java-metrics.git
```

**2. Build da aplicaçāo maven**

```bash
mvn clean package
```

**3. Roda a aplicaçāo**
```bash
mvn package
java -jar target/actuator-demo-0.0.1-SNAPSHOT.jar
```

Alternatively, you can run the app directly without packaging like this -

```bash
mvn spring-boot:run
```

**4. Rodando toda a stack**

```bash
docker-compose up --build
```


**5. Retagueando imagens e subindo no repositorio**

```bash
docker tag java-metrics_service-java -t user/java-app:1.0.0
```

```bash
docker tag java-metrics_prometheus -t user/prometheus:1.0.0
```

```bash
docker tag java-metrics_grafana -t user/grafana:1.0.0
```


## Explorar terminais do Endpoints

Todos actuator endpoints estará disponível em <http://localhost:8080/actuator>.

prometheus (queries): http://127.0.0.1:9090
grafana (graficos): http://127.0.0.1:3000


Alguns dos actuator endpoints são protegidos com a autenticação HTTP Basic da Spring Security. Você pode usar o nome de usuário `actuator` e senha `actuator` para autenticação básica de http.


